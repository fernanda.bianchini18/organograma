create table node(
    id bigint primary key auto_increment,
    unidade_id bigint,
    departamento_id bigint,
    funcao_id bigint,
    vinculo_id bigint,
    organograma_id bigint,
    node_parent_id bigint,

    foreign key (unidade_id) references unidade(id),
    foreign key (departamento_id) references departamento(id),
    foreign key (funcao_id) references funcao(id),
    foreign key (vinculo_id) references vinculo(id),
    foreign key (organograma_id) references organograma(id),
    foreign key (node_parent_id) references node(id)
);