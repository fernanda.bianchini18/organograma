insert into node (organograma_id, unidade_id)
select (select id from organograma where ativa = true), id
from unidade;


SET @num_unidade := (SELECT COUNT(*)
                     FROM unidade);
SET @num_departamento := (SELECT COUNT(*)
                          FROM departamento);
SET @count := 0;

INSERT INTO node (organograma_id, departamento_id, node_parent_id)
SELECT (SELECT id FROM organograma WHERE ativa = true),
       departamento.id,
       unidade_node.id
FROM departamento
         JOIN
     (SELECT node.id, (@count := @count + 1) AS count
      FROM node,
           (SELECT @count := 0) AS count
      WHERE unidade_id IS NOT NULL
      ORDER BY id) AS unidade_node
     ON
         unidade_node.count % @num_unidade = departamento.id % @num_unidade;



SET @num_departamento := (SELECT COUNT(*)
                          FROM departamento);
SET @num_funcao := (SELECT COUNT(*)
                    FROM funcao);
SET @count := 0;

INSERT INTO node (organograma_id, funcao_id, node_parent_id)
SELECT (SELECT id FROM organograma WHERE ativa = true),
       funcao.id,
       departamento_node.id
FROM funcao
         JOIN
     (SELECT node.id, (@count := @count + 1) AS count
      FROM node,
           (SELECT @count := 0) AS count
      WHERE departamento_id IS NOT NULL
      ORDER BY id) AS departamento_node
     ON
                 departamento_node.count % @num_departamento = funcao.id % @num_departamento;


SET @num_funcoes := (SELECT COUNT(*)
                     FROM funcao);
SET @num_vinculos := (SELECT COUNT(*)
                      FROM vinculo);
SET @count := 0;
SET @vcount := 0;

INSERT INTO node (organograma_id, vinculo_id, node_parent_id)
select t.organograma_id, t.vinculo_id, t.node_parent_id from (
SELECT (SELECT id FROM organograma WHERE ativa = true) as organograma_id,
       v.id as vinculo_id,
       funcao_node.id as node_parent_id,
       (@vcount := @vcount + 1) AS vcount
FROM vinculo v,
     (SELECT @vcount := 0) AS vcount
         JOIN
     (SELECT node.id, (@count := @count + 1) AS count
      FROM node,
           (SELECT @count := 0) AS count
      WHERE funcao_id IS NOT NULL
      ORDER BY id) AS funcao_node
     ON
                @vcount % funcao_node.count = 0
       ) as t;